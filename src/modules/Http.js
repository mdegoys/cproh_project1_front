let url;

if (process.env.NODE_ENV === 'production') {
	url = 'https://cproh-project1.herokuapp.com';
} else {
	url = 'http://localhost:4000'; 
}

export default {

	get: (route) => {
		return fetch(url + '/' + route)
		.then(res => res.json())
	},

	post: (route, body, token = '') => {
		const headers = {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
		}
		if (token !== '') {
			headers.token = token;
		}
		return fetch(url + '/' + route,
				{ method : 'POST',
					headers,
					body: JSON.stringify(body)
			})
		.then(res => res.json())
	},

	put: (route, body) => {
		return fetch(url + '/' + route,
			{ method : 'PUT',
				headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    		body: JSON.stringify(body)
		})
		.then(res => res.json())
	},

	delete: (route) => {
		return fetch(url+ '/' + route, { method : 'DELETE' })
		.then(res => res.json())
	}
}
