import React, { Component } from "react";
import { Link } from "react-router-dom";
import Http from '../modules/Http';
import ResultMessage from './ResultMessage';

class DisplayUsers extends Component {
	state = {
		users: [],
	}

	getUsersData = () => {
		Http.get('users')
		.then(data => this.setState({ users: data.users }))
	}

	componentDidMount() {
		this.getUsersData();
	}

	componentDidUpdate(prevProps) {
		if (this.props.location.state && this.props.location.state.msg && prevProps.msg === '') {
			this.getUsersData();
		}
	}

	render() {
		console.log(this.props.location.state);
		return (
			<div>
			{ this.props.location.state && this.props.location.state.msg && <ResultMessage msg={this.props.location.state.msg} /> }
				<h2>List of Users</h2>
				<div className="collection">
					{ this.state.users.map((user, i) =>
						<Link key={i} className="collection-item" to={'/user/'+user.userid}>{user.username}</Link>
					)}
				</div>
			</div>
		)
	}
}
	
export default DisplayUsers;
