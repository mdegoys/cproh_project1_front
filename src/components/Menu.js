import React from "react";
import { Link } from "react-router-dom";

const Menu = (props) => (
    <div>
      <nav>
        <ul>
         <li>
          	<Link to="/users">Users</Link>
         </li>
         <li>
						<Link to="/users/add">Add a user</Link>
          </li>
         {/*<li>
						<Link to="/login">Login</Link>          
					</li>*/}
        </ul>
      </nav>

    </div>
);

export default Menu;

