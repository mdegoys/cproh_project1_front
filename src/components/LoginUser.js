import React, { Component } from "react";

import FormUser from './FormUser';
import Http from '../modules/Http';
import ResultMessage from './ResultMessage';

class LoginUser extends Component {
	state = {
		dataUser: 
		{ username: '',
			password: ''
		},
		resultMessage: ''
	}

	updateData = (e) => {
		this.setState({ dataUser : {...this.state.dataUser, [e.target.id] : e.target.value}  });
	}

	loginUser = (e) => {
		e.preventDefault();
		Http.post('users/login', this.state.dataUser)
		.then(data => {
			if (data.result === 'success') {
				localStorage.setItem('token', data.token);
			} else {
				this.setState({ resultMessage : data.msg });			}
		})
	}

	render() {
		return (
			<div>
				{ this.state.resultMessage ? <ResultMessage msg={this.state.resultMessage} /> : '' }
				<h2>Login</h2>
				<FormUser edit={false} data={this.state.dataUser} actionUpdate={this.updateData} actionSubmit={this.loginUser} />
			</div>
		)
	}
}
	
export default LoginUser;
