import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";
import Menu from './Menu';
import DisplayUser from './DisplayUser';
import DisplayUsers from './DisplayUsers';
import AddUser from './AddUser';
import LoginUser from './LoginUser';
import './App.css';

class App extends Component {
	
  render() {
    return (
			<Router basename={process.env.PUBLIC_URL}>
				<div className="App">
					<Menu />

      		<div id="main">
						<Switch>
							<Route path="/user/:id" exact component={DisplayUser} />
							<Route path="/users" exact component={DisplayUsers} />
							<Route path="/users/add" exact component={AddUser} />
							<Route path="/login" exact component={LoginUser} />
							<Redirect from="/" to="/users" />
						</Switch>
					</div>
				</div>
			</Router>
    );
  }
}

export default App;
