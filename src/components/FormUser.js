import React, { Component } from "react";

class FormUser extends Component {

	 additionnalInputs = () => { 
		return (
			<div>
				<div>
					<label htmlFor="firstname">First name</label>
					<input id="firstname" type="text" value={this.props.newData.firstname || ''} onChange={this.props.actionUpdate} />
				</div>

				<div>
					<label htmlFor="lastname">Last name</label>
					<input id="lastname" type="text" value={this.props.newData.lastname || ''} onChange={this.props.actionUpdate} />
				</div>

				<div>
					<label htmlFor="avatarurl">Avatar url</label>
					<input id="avatarurl" type="text" value={this.props.newData.avatarurl || ''} onChange={this.props.actionUpdate} />
				</div>

			{/*<div>
					<label htmlFor="birthday">Birthday</label>
					<input id="birthday" type="date" value={this.props.newData.birthday || ''} onChange={this.props.actionUpdate} />
				</div>*/}

				<div>
					<label htmlFor="city">City</label>
					<input id="city" type="text" value={this.props.newData.city || ''} onChange={this.props.actionUpdate} />
				</div>

				<div>
					<label htmlFor="country">Country</label>
					<input id="country" type="text" value={this.props.newData.country || ''} onChange={this.props.actionUpdate} />
				</div>

				<div>
					<label htmlFor="postalcode">Postal code</label>
					<input id="postalcode" type="text" value={this.props.newData.postalcode || ''} onChange={this.props.actionUpdate} />
				</div>
				<button type="submit" className="btn waves-effect waves-light" onClick={(e) => this.props.actionSubmit(e)}>Submit <i className="material-icons right">send</i></button>

			</div>
		)
	};

	simpleSubmit = () => {
		return (
			<div>
				<button type="submit" className="btn waves-effect waves-light" onClick={this.props.actionSubmit}>Submit <i className="material-icons right">send</i></button>
			</div>
		)
	}

	render () {
		return (
		<form>
			<div>
				<label htmlFor="username">Username (valid email address) *</label>
				<input id="username" type="text" value={this.props.edit ? this.props.newData.username : this.props.data.username}  disabled={this.props.edit ? 'disabled': '' } onChange={this.props.actionUpdate} />
			</div>

			<div>
				<label htmlFor="password">Password *</label>
				<input id="password" type="password" value={this.props.edit ? this.props.newData.password : this.props.data.password} disabled={this.props.edit ? 'disabled': '' } onChange={this.props.actionUpdate} />
			</div>

			{this.props.edit && this.additionnalInputs()}
			{!this.props.edit && this.simpleSubmit()}
			
		</form>
		)
	}
}

export default FormUser;
