import React, { Component } from "react";
import { Redirect } from 'react-router-dom';
import FormUser from './FormUser';
import Http from '../modules/Http';
import ResultMessage from './ResultMessage';

class DisplayUser extends Component {
	state = {
		edit: false,
		newUserData:
		{ 
			username: '',
			password: '',
			firstname: '',
			lastname: '',
			avatarurl: '',
			birthday: '',
			city: '',
			country: '',
			postalcode: ''
		},
		resultStatus: '',
		resultMessage: ''
	}

	getUserData() {
		console.log('nmaj');
		Http.get('users/'+this.props.match.params.id)
		.then(data => this.setState({ newUserData: data.user }))
	}

	componentDidMount() {
		this.getUserData();
	}

	componentDidUpdate(prevProps, prevState) {
		if (this.state.resultMessage !== prevState.resultMessage && this.state.resultMessage !== 'The user has been deleted') {
			this.getUserData();
		}
	}

	toggleEdit = () => {
		this.setState({ resultStatus: '', resultMessage: '', edit : !this.state.edit });
	}

	updateData = (e) => {
		this.setState({ newUserData : {...this.state.newUserData, [e.target.id] : e.target.value} });
	}

	editUser = (e) => {
		e.preventDefault();
		Http.put('users/'+this.props.match.params.id, this.state.newUserData)
		.then(data => {
			console.log('result:', JSON.stringify(data))
			this.setState({ resultStatus: data.result ,resultMessage: data.msg, edit: !this.state.edit });
		})
		.catch(err => console.log('erreur:', err))
	}

	deleteUser = (e) => {
		e.preventDefault();
		console.log('delete');
		Http.delete('users/'+this.props.match.params.id)
		.then(data => {
			this.setState({ resultStatus: data.result ,resultMessage: data.msg });
		})
		.catch(err => console.log(err))

	}

	render() {
		if (!this.state.edit) {
			return (
				<div>
					{ this.state.resultMessage ? this.state.resultMessage === 'The user has been deleted' ? <Redirect to={{ pathname:'/users', state: { msg:this.state.resultMessage } }} /> : <ResultMessage msg={this.state.resultMessage} /> : '' }
					<h2>User details</h2>
					<ul>
						<li><strong>id</strong>:	{this.props.match.params.id}</li>
						<li><strong>Username</strong>: {this.state.newUserData.username}</li>
						<li><strong>First name</strong>:	{this.state.newUserData.firstname || 'not filled in'}</li>
						<li><strong>Last name</strong>:	{this.state.newUserData.lastname || 'not filled in'}</li>
						<li><strong>Avatar url</strong>:	{this.state.newUserData.avatarurl || 'not filled in'}</li>
				{/*<li><strong>Birthday</strong>:	{this.state.newUserData.birthday || 'not filled in'}</li>*/}
						<li><strong>City</strong>:	{this.state.newUserData.city || 'not filled in'}</li>
						<li><strong>Country</strong>:	{this.state.newUserData.country || 'not filled in'}</li>
						<li><strong>Postal code</strong>:	{this.state.newUserData.postalcode || 'not filled in'}</li>
					</ul>

					<button className="btn waves-effect waves-light" onClick={this.toggleEdit}>Edit this user</button> <button className="btn waves-effect waves-light" onClick={this.deleteUser}>Delete this user</button>

				</div>
			)
		} else {
			return (
				<div>
					<h2>Edit user details</h2>
					<FormUser edit={true} newData={this.state.newUserData} actionUpdate={this.updateData} actionSubmit={this.editUser} />
				</div>
			)
		}
	}
}	

export default DisplayUser;
