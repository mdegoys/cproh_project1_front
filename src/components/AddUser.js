import React, { Component } from "react";
import { Redirect } from 'react-router-dom';

import FormUser from './FormUser';
import Http from '../modules/Http';
import ResultMessage from './ResultMessage';


class AddUser extends Component {
	state = {
		newUser: 
		{ username: '',
			password: ''
		},
		resultStatus: '',
		resultMessage: '',
	}

	updateData = (e) => {
		this.setState({ newUser : {...this.state.newUser, [e.target.id] : e.target.value}  });
	}

	addUser = (e) => {
		e.preventDefault();
		Http.post('users', this.state.newUser)	
		.then(data => {	
			console.log(JSON.stringify(data)) 
			this.setState({ resultStatus: data.result ,resultMessage: data.msg });
		})
		.catch(err => console.log(err))
	}

	render() {
		return (
			<div>
				{ this.state.resultMessage ? this.state.resultStatus === 'success' ? <Redirect to={{ pathname:'/users', state: { msg:this.state.resultMessage } }} /> : <ResultMessage msg={this.state.resultMessage} /> : '' }
				<h2>Add a user</h2>
				<FormUser edit={false} data={this.state.newUser} actionUpdate={this.updateData} actionSubmit={this.addUser} />
			</div>
		)
	}
}
	
export default AddUser;
